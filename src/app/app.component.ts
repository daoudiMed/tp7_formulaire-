import { Component, OnInit } from '@angular/core';
import {FormBuilder , FormGroup, FormControl } from '@angular/forms'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  //title = 'TP7AngularForm';
  myForm : FormGroup;
  formList: FormControl[] = [];

  ngOnInit(){}

  constructor(fb:FormBuilder){
    this.myForm = fb.group({
      'nom': new FormControl(),
      'age': new FormControl(),
      'ville': new FormControl(),
    });
  }

  afficherForm(){
    alert(JSON.stringify(this.myForm.value));
  }

  enregistrer(myForm){
    this.formList.push(myForm);
  }

}
